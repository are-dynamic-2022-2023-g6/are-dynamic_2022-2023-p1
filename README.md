# ARE DYNAMIC-G6 - Évacuation et handicap

Ce projet a été réalisé par YOUSFI Rayane, MOTTA Raphaël et HADDAB Mayes, élèves en L1 portail ScFo (Sciences Formelles), au campus de Jussieu de Sorbonne Université.

## Description du projet

Notre travail de recherche documentaire consiste à comprendre l'influence des personnes aveugles au sein d'une évacuation. Les personnes aveugles ayant des spécificités physiques que d'autres personnes n'auront pas, et une évacuation étant concevable comme un labyrinthe 2D,
nous nous sommes intéressés à modéliser cet événement.

## Site web

Le site web n'est pas fini, mais voilà: https://are-dynamic-2022-2023-g6.gitlab.io/are-dynamic_2022-2023-p1/

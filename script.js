const start = document.querySelector(".start");
const after = document.querySelector(".content");
const elemetsArray = document.querySelectorAll(".dot");

const videoArray = [
    "Site/Aresans_av.mp4",
    "Site/areaveugle.mp4",
    "Site/SoutenanceARE.mp4",
];

let activeIndex = 0;

function animation() {
    setTimeout(() => {
        start.classList.remove("opacity");
        setTimeout(() => {
            start.remove();
            after.classList.add("opacity");
        }, 500);
    }, 3000);
};

const selectActive = (el) => {
    elemetsArray[activeIndex].classList.remove("active");
    el.classList.add("active");
    activeIndex = [...elemetsArray].indexOf(el);
    window.dispatchEvent(new CustomEvent("change", { detail: activeIndex }));
};


const main = () => {
    animation();
    for (const el of elemetsArray) {
        el.addEventListener("click", () => selectActive(el));
    };
};


const changeContent = (e) => {
    const content = [document.querySelector(".content1"), document.querySelector(".content2"), document.querySelector(".content3")];
    const video = document.querySelector('video');
    const source = document.createElement('source');
    video.innerHTML = '';

    source.setAttribute('src', videoArray[e.detail]);
    source.setAttribute('type', 'video/mp4');

    video.appendChild(source);
    video.load();
    video.play();

    content[e.detail].classList.remove("d-none");
    for (const el of content) {
        if (el !== content[e.detail]) {
            el.classList.add("d-none");
        };
    }
};

window.addEventListener("change", changeContent);
document.addEventListener("DOMContentLoaded", main);
